﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Archiver.CommandLineParser.Commands;

namespace Archiver.ApplicationStates
{
    public interface IArgumentData
    {
        string[] Arguments
        {
            get;
            set;
        }

        ICommand Command
        {
            get;
            set;
        }
    }
}
