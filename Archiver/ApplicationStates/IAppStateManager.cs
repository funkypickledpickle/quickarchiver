﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Archiver.ApplicationStates.States;

namespace Archiver.ApplicationStates
{
    public interface IAppStateManager : IArgumentData
    {
        IAppState[] AvailableStates
        {
            get;
        }

        IAppState CurrentState
        {
            get;
        }

        void StartStateManager ();
        void Iterate ();
        void StopStateManager ();
        void SwitchState (IAppState state);
    }
}
