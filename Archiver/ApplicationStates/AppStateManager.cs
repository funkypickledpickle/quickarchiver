﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Archiver.ApplicationStates.States;
using Archiver.CommandLineParser.Commands;

namespace Archiver.ApplicationStates
{
    public class AppStateManager : IAppStateManager
    {
        private bool _isAppManagerWorking = false;

        public string[] Arguments
        {
            get;
            set;
        }

        public ICommand Command
        {
            get;
            set;
        }

        public IAppState[] AvailableStates
        {
            get;
            private set;
        }

        public IAppState CurrentState
        {
            get;
            private set;
        }

        public AppStateManager (string[] arguments)
        {
            Arguments = arguments;
            AvailableStates = new IAppState[]
            {
                new ParseCommandLineState (this),
                new ExecuteCommandState (this),
                new ExceptionState (this), 
            };
            CurrentState = AvailableStates[0];
        }

        public void StartStateManager ()
        {
            _isAppManagerWorking = true;
        }

        public void Iterate ()
        {
            while (_isAppManagerWorking && CurrentState != null)
            {
                CurrentState.ExecuteState ();
            }
        }

        public void StopStateManager ()
        {
            _isAppManagerWorking = false;
        }

        public void SwitchState (IAppState state)
        {
            CurrentState = state;
        }
    }
}
