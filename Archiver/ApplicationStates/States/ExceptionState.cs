﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Archiver.CommandLineParser.Commands;

namespace Archiver.ApplicationStates.States
{
    public class ExceptionState : IAppState
    {
        private IAppStateManager _appStateManager = null;

        public ExceptionState (IAppStateManager appStateManager)
        {
            _appStateManager = appStateManager;
        }

        public void ExecuteState ()
        {
            Console.WriteLine(new HelpCommand ().HelpOperation ());
            _appStateManager.StopStateManager ();
            Environment.ExitCode = 1;
            return;
        }
    }
}
