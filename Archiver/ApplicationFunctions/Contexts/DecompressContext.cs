﻿using System;
using System.IO;
using System.IO.Compression;
using System.Runtime.ExceptionServices;
using System.Threading;
using Archiver.Archiver;
using Archiver.CommandLineParser.Commands;

namespace Archiver.Application.Contexts
{
    public class DecompressContext : IAppContext
    {
        private ManualResetEvent _manualResetEvent = null;
        private OperationResult operationResult;

        public void Execute (ICommand command)
        {
            _manualResetEvent = new ManualResetEvent (false);
            var compressCommand = command as DecompressCommand;
            var parsedData = compressCommand.ParsedData.Value;
            FileStream inputStream = File.OpenRead (parsedData.InputPath);
            FileStream outputStream = File.Create (parsedData.OutputPath);
            IArchiver archiver = new FastArchiver (inputStream, outputStream, CompressionMode.Decompress, OnComplete);
            archiver.ExecuteAsync ();
            _manualResetEvent.WaitOne ();

            inputStream.Dispose();
            outputStream.Dispose();

            if (!operationResult.IsCompleted)
            {
                if (operationResult.Exception != null)
                {
                    ExceptionDispatchInfo.Capture (operationResult.Exception).Throw ();
                }
                else
                {
                    throw new Exception ("uknown error");
                }
            }
        }

        private void OnComplete (OperationResult operationResult)
        {
            this.operationResult = operationResult;
            _manualResetEvent.Set ();
        }
    }
}
