﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Archiver.ApplicationStates;
using Archiver.Archiver;
using Microsoft.VisualBasic;
using Archiver.CommandLineParser;
using Microsoft.VisualBasic.Devices;

namespace Archiver
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ExecuteApp (args);
            //QuickTest ();
            //Console.ReadLine ();
        }

        private static void ExecuteApp (string[] args)
        {
            IAppStateManager appStateManager = new AppStateManager (args);
            appStateManager.StartStateManager ();
            appStateManager.Iterate ();
        }

        private static void QuickTest ()
        {
            ExecuteApp (new string[] { "compress", "original", "compressed" });
            ExecuteApp (new string[] { "decompress", "compressed", "copy" });
            ExecuteApp (new string[] { "help", "wrong1", "wrong2" });
            ExecuteApp (new string[] { "help"});
        }
    }
}
