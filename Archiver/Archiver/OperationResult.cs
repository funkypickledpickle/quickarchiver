﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Archiver.Archiver
{
    public struct OperationResult
    {
        public Exception Exception;
        public bool IsCompleted;
    }
}
