﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Archiver.Archiver.BlockOperations;
using Archiver.Archiver.Misc;
using Archiver.SimpleObjectPool;
using Microsoft.VisualBasic.Devices;

namespace Archiver.Archiver
{
    public class FastArchiver : IArchiver, IDataContainer
    {
        public const int MegaByte = 1024 * 1024;
        public const int MinBlockSize = MegaByte;
        public readonly float MemoryUsageFactor = 0.1f;
        public readonly float MaxMemoryUsageFactor = 0.5f;
        public readonly int MaxBlockSize = 256 * MegaByte;
        public readonly float BlockSizeMultiplier = 2;

        public event Action<OperationResult> OnFinished = null;

        private Object _exceptionLockObject = null;
        private bool _isAbortStarted = false;

        private readonly Stream _inputStream = null;
        private readonly Stream _outputStream = null;
        private int _blockSize = 0;
        private ulong _aviableMemorySize = 0;
        private ComputerInfo _computerInfo = null;

        private LinkedList<BlockData> _operationsQueue = null;
        private List<Thread> _threads = null;
        private CompressionMode _compressionMode;
        private IBlockOperation _blockOperation = null;

        private readonly int _maxthreadsCount = 0;
        private IObjectPool<MemoryStream> _memoryStreamPool = null; 

        public bool IsExecutionCompleted
        {
            get;
            private set;
        }

        public bool IsStreamFinished
        {
            get
            {
                lock (_inputStream)
                {
                    return !_inputStream.CanRead || _inputStream.Position >= _inputStream.Length;
                }
            }
        }

        public Stream InputStream
        {
            get
            {
                return _inputStream;
            }
        }

        public Stream OutputStream
        {
            get
            {
                return _outputStream;
            }
        }

        public int BlockSize
        {
            get
            {
                return _blockSize;
            }
        }

        public IBlockOperation BlockOperation
        {
            get
            {
                return _blockOperation;
            }
        }

        #region Initialization
        public FastArchiver (Stream inputStream, Stream outputStream, CompressionMode compressionMode, Action<OperationResult> onFinished)
        {
            IsExecutionCompleted = false;

            _inputStream = inputStream;
            _outputStream = outputStream;
            _compressionMode = compressionMode;
            _maxthreadsCount = Environment.ProcessorCount;
            _memoryStreamPool = new MemoryStreamPool ();  
            _computerInfo = new ComputerInfo();
            _aviableMemorySize = _computerInfo.AvailablePhysicalMemory;
            OnFinished = onFinished;

            _blockOperation = GetBlockOperation (compressionMode);    
            _blockSize = CalculateBlockSize ();               
        }

        private IBlockOperation GetBlockOperation (CompressionMode compressionMode)
        {
            switch (compressionMode)
            {
                case CompressionMode.Compress:
                {
                    return new Compression (this);
                }
                case CompressionMode.Decompress:
                {
                    return new Decompression (_memoryStreamPool, this);
                }
                default:
                {
                    throw new NotImplementedException ();
                }
            }
        }

        private int CalculateBlockSize()
        {
            var sizeHelper = new BlockSizeHelper (MinBlockSize, MaxBlockSize, MemoryUsageFactor, _blockOperation.MemoryUsageFactor);
            return sizeHelper.GetAverageBlockSize (_inputStream.Length);
        }

#endregion

#region Execution
        public void ExecuteAsync ()
        {
            InitializeBlockOperation ();
            _operationsQueue = new LinkedList<BlockData> ();
            _threads = new List<Thread> ();
            _exceptionLockObject = new object ();
            _isAbortStarted = false;

            long totalSize = 0;
            for (int i = 0; i < _maxthreadsCount && _inputStream.Length > totalSize; i++)
            {
                CreateThreadOperation ();
                totalSize += BlockSize;
            }                       
        }

        private void InitializeBlockOperation ()
        {
            lock (_inputStream) lock (_outputStream) lock (_blockOperation) 
            {
               _blockOperation.Initialize(this);
            }
        }

        private void FinalizeBlockOperation()
        {
            lock (_inputStream) lock (_outputStream) lock (_blockOperation) 
            {
                _blockOperation.Finalize(this);
            }
        }

        private void ExecutionCompleted ()
        {
            lock (_inputStream) lock (_outputStream) lock (_operationsQueue)
            {
                if (IsExecutionCompleted)
                {
                    return;
                }
                IsExecutionCompleted = true;

                FinalizeBlockOperation();
                ReleaseData ();

                if (OnFinished != null)
                {
                    OnFinished.Invoke (new OperationResult ()
                    {
                        Exception = null,
                        IsCompleted = true,
                    });
                }
            }
        }
#endregion

#region ExecutionSteps
        private void ExecuteOperationSteps (object currentThread)
        {
            LinkedListNode<BlockData> node = null;
            try
            {
                lock (_inputStream)
                {
                    if (IsExecutionCompleted)
                    {
                        return;
                    }
                    node = StartOperation(currentThread as Thread);
                }

                if (node == _operationsQueue.Last)
                {
                    CreateExtraThread ();
                }

                ExecuteOperation (node);
                FinishOperation (node);

                lock (_threads)
                {
                    _threads.Remove(currentThread as Thread);
                }

                if (IsCompleted ())
                {
                    ExecutionCompleted ();
                    return;
                }
                else if (!IsStreamFinished)
                {
                    CreateThreadOperation();
                }
            }
            catch (ThreadAbortException)
            {
                Console.WriteLine("Thread aborted");
                return;
            }
            catch (Exception exception)
            {
                lock (_exceptionLockObject)
                {
                    if (_isAbortStarted || IsExecutionCompleted)
                    {
                        return;
                    }
                    _isAbortStarted = true;

                    Thread thread = new Thread (() => AbortExecution (exception));
                    thread.Start ();
                    return;
                }
            }
        }

        [MethodImpl (MethodImplOptions.Synchronized)]
        private void AbortExecution (Exception exception)
        {
            AbortThreads ();
            ReleaseData ();
            if (OnFinished != null)
            {
                OnFinished.Invoke (new OperationResult ()
                {
                    Exception = exception,
                    IsCompleted = false,
                });
            }
        }

        private LinkedListNode<BlockData> StartOperation (Thread currentThread)
        {
            LinkedListNode<BlockData> node = null;

            lock (_inputStream)
            {
                byte[] bytes = null;
                int readLength;
                IBlockOperation blockOperation = null;
                lock (_blockOperation)
                {
                    blockOperation = _blockOperation;
                }
                blockOperation.ReadBlock (out bytes, out readLength, _inputStream);

                var data = new BlockData ()
                {
                    OutputStream = _memoryStreamPool.Get (),
                    OperationState = OperationState.Idle,
                    InputData = bytes,
                    ReadLength = readLength,
                    CompressionMode = _compressionMode,
                    BlockOperation = blockOperation,
                    DataContainer = this,
                };

                node = new LinkedListNode<BlockData> (data);

                lock (_operationsQueue)
                {
                    _operationsQueue.AddLast (node);  
                }
            }
            return node;
        }

        private void ExecuteOperation (LinkedListNode<BlockData> node)
        {
            node.Value.BlockOperation.Execute (node);
        }

        private void FinishOperation (LinkedListNode<BlockData> node)
        {           
            WriteBlocks ();
        }

#endregion

#region Operations
        private bool IsCompleted ()
        {
            lock (_inputStream) lock (_outputStream) lock (_operationsQueue) lock(_threads)
            {
                if (IsExecutionCompleted || (IsStreamFinished && _operationsQueue.Count == 0 && _threads.Count == 0))
                {
                    return true;
                }
            }
            return false;
        }

        private void WriteBlocks()
        {
            lock (_outputStream)
            {
                BlockData blockData = null;
                while ((blockData = GetFirstFinishedBlock(_operationsQueue)) != null)
                {
                    blockData.BlockOperation.WriteBlock(blockData, _outputStream);
                    _memoryStreamPool.Put(blockData.OutputStream as MemoryStream);
                    blockData.OutputStream = null;
                }
            }
        }

        private BlockData GetFirstFinishedBlock(LinkedList<BlockData> operationsQueue)
        {
            lock (operationsQueue)
            {
                if (operationsQueue.Count > 0 &&
                    operationsQueue.First.Value.OperationState == OperationState.Finished)
                {
                    var block = operationsQueue.First.Value;
                    operationsQueue.RemoveFirst();
                    return block;
                }
            }
            return null;
        }

        private void CreateThreadOperation ()
        {
            lock (_threads)
            {
                Thread thread = new Thread (ExecuteOperationSteps);
                _threads.Add(thread);
                thread.Start (thread);
            }
        }

        private void CreateExtraThread ()
        {
            if (!IsExecutionCompleted && !IsStreamFinished)
            {
                if (!Monitor.IsEntered (_inputStream) && _threads.Count * _blockOperation.MemoryUsage < _aviableMemorySize * MaxMemoryUsageFactor)
                {
                    CreateThreadOperation ();
                }
            }
        }

        private void ReleaseData ()
        {
            _memoryStreamPool.Release ();
            _blockOperation.Release ();
        }

        public void IncreaseBlockSize(int? size)
        {
            if (_inputStream.Length < _blockSize)
            {
                return;
            }
            lock (_inputStream) lock (_outputStream) lock (_blockOperation)
            {
                _blockSize = size ?? (int)(_blockSize * BlockSizeMultiplier);
                _blockOperation = GetBlockOperation(_compressionMode);
                InitializeBlockOperation();
            }     
        }

        private void AbortThreads ()
        {
            lock (_threads)
            {
                foreach (var thread in _threads)
                {
                    thread.Abort();
                    thread.Join ();
                }
            }
        }
#endregion
    }
}
