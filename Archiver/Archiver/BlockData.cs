﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Archiver.Archiver.BlockOperations;

namespace Archiver.Archiver
{
    public enum OperationState
    {
        Idle,
        Running,
        Finished
    }

    public class BlockData
    {
        public OperationState OperationState;
        public byte[] InputData;
        public Stream OutputStream;
        public CompressionMode CompressionMode;
        public int ReadLength;
        public IBlockOperation BlockOperation;
        public IDataContainer DataContainer;
    } 
}
