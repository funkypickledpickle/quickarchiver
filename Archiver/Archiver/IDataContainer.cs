﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Archiver.Archiver.BlockOperations;

namespace Archiver.Archiver
{
    public interface IDataContainer
    {
        Stream InputStream
        {
            get;
        }

        Stream OutputStream
        {
            get;
        }

        int BlockSize
        {
            get;
        }

        IBlockOperation BlockOperation
        {
            get;
        }

        void IncreaseBlockSize (int? size);
    }
}
