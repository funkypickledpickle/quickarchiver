﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Archiver.Archiver.BlockOperations
{
    [StructLayout(LayoutKind.Explicit)]
    public struct GZipHeader
    {
        [FieldOffset(0)]
        public long MaxBlockSize;
        [FieldOffset(8)]
        public long BlockIndex; // не используется
    }
}
