﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Archiver.Archiver.BlockOperations
{
    public interface IBlockOperation : IBlockInfo
    {
        void Initialize(IDataContainer dataContainer);
        void Finalize(IDataContainer dataContainer);
        
        void Release ();
        void ReadBlock (out byte[] bytes, out int readLength, Stream inputStream);
        void WriteBlock (BlockData blockData, Stream outputStream);
        void Execute (LinkedListNode<BlockData> operationNode);
    }
}
