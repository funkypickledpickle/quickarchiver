﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Runtime.InteropServices;
using Archiver.SimpleObjectPool;

namespace Archiver.Archiver.BlockOperations
{
    public class Compression : IBlockOperation
    {
        private readonly float _memoryUsageFactor = 3.5f;

        private IObjectPool<byte[]> _byteArrayPool = null;
        private int _blockSize = 0;
        private long _maxEncryptedBlockSize = 0;

        public long MemoryUsage
        {
            get
            {
                return (long)(_blockSize * MemoryUsageFactor + 0.5f);
            }
        }

        public float MemoryUsageFactor
        {
            get
            {
                return _memoryUsageFactor;
            }
        }

        public Compression (IDataContainer dataContainer)
        {

        }

        public void Initialize(IDataContainer dataContainer)
        {
            _blockSize = dataContainer.BlockSize;
            _maxEncryptedBlockSize = _blockSize;
            _byteArrayPool = new ArrayObjectPool<byte> (dataContainer.BlockSize);

            byte[] bytes = null;
            GZipHeader gZipHeader = new GZipHeader ();
            HeaderOperations.Copy (gZipHeader, out bytes);
            dataContainer.OutputStream.Seek(0, SeekOrigin.Begin);
            dataContainer.OutputStream.Write (bytes, 0, bytes.Length);
            dataContainer.OutputStream.Seek(0, SeekOrigin.End);
            Console.WriteLine (string.Format ("BlockSize set to {0} MB", (float) _blockSize / FastArchiver.MegaByte));
        }

        public void Finalize(IDataContainer dataContainer)
        {
            byte[] bytes = null;
            GZipHeader gZipHeader = new GZipHeader ()
            {
                BlockIndex = 0,
                MaxBlockSize = _maxEncryptedBlockSize,
            };
            HeaderOperations.Copy (gZipHeader, out bytes);
            dataContainer.OutputStream.Seek (0, SeekOrigin.Begin);
            dataContainer.OutputStream.Write (bytes, 0, bytes.Length);
            dataContainer.OutputStream.Seek (0, SeekOrigin.End);
        }

        public void Release()
        {
            _byteArrayPool.Release();
        }

        public void ReadBlock (out byte[] bytes, out int readLength, Stream inputStream)
        {
            bytes = _byteArrayPool.Get ();
            readLength = _blockSize;
            if (readLength + inputStream.Position > inputStream.Length)
            {
                readLength = (int) (inputStream.Length - inputStream.Position);
            }
            inputStream.Read (bytes, 0, readLength);
        }

        public void WriteBlock (BlockData blockData, Stream outputStream)
        {
            var bytes = BitConverter.GetBytes (blockData.OutputStream.Length);
            outputStream.Write (bytes, 0, sizeof(long));
            blockData.OutputStream.Seek (0, SeekOrigin.Begin);
            blockData.OutputStream.CopyTo (outputStream);
        }

        public void Execute (LinkedListNode<BlockData> operationNode)
        {
            var operationData = operationNode.Value;
            operationData.OperationState = OperationState.Running;
            using (GZipStream compressionStream = new GZipStream (operationData.OutputStream, operationData.CompressionMode, true))
            {
                compressionStream.Write (operationData.InputData, 0, operationData.ReadLength);
            }

            if (operationData.OutputStream.Length >= operationData.InputData.Length && operationData.DataContainer.BlockOperation == operationData.BlockOperation)
            {
                operationData.DataContainer.IncreaseBlockSize (null);
            }

            _byteArrayPool.Put (operationData.InputData);
            operationData.InputData = null;

            if (operationData.OutputStream.Length > _maxEncryptedBlockSize)
            {
                _maxEncryptedBlockSize = operationData.OutputStream.Length;
            }
            operationData.OperationState = OperationState.Finished;
        }
    }
}
