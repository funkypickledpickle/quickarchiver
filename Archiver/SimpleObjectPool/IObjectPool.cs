﻿namespace Archiver.SimpleObjectPool
{
    public interface IObjectPool<Type>
    {
        Type Get ();
        void Put (Type value);

        void Release ();
    }
}
