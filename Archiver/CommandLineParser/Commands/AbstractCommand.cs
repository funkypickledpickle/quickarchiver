﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Archiver.CommandLineParser.Commands
{
    public abstract class AbstractCommand : ICommand
    {
        protected readonly string _operationWrongSymbols = @"(\s+|@|&|'|\(|\)|<|>|#)";

        #region CommandProperties
        public abstract string GetCommandName ();
        public abstract int GetArgumentsAmount ();
        #endregion CommandProperties

        #region ICommand
        public virtual bool IsCommandNameMatch (string[] args)
        {
            if (args.Length == 0 || !(Regex.Replace (args[0].ToLower (), _operationWrongSymbols, "")).Equals (GetCommandName()))
            {
                return false;
            }
            return true;
        }

        public virtual bool CheckArguments (string[] args)
        {
            return IsCommandNameMatch (args) && args.Length == GetArgumentsAmount();
        }

        public abstract void Parse(string[] args);
        public abstract bool TryParse(string[] args);   
        #endregion
    }
}
