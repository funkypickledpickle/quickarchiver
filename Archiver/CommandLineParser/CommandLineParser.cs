﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Text;
using Archiver.CommandLineParser.Commands;

namespace Archiver.CommandLineParser
{
    public class CommandLineParser : ICommandLineParser
    {
        private ICommand[] _commandsList = null;

        public CommandLineParser ()
        {
            _commandsList = new ICommand[]
            {
                new CompressCommand(), 
                new DecompressCommand(), 
                new HelpCommand(), 
            };
        }

        public ICommand ParseCommand(string[] args)
        {
            foreach (var command in _commandsList)
            {
                if (command.IsCommandNameMatch (args))
                {
                    return command;
                }
            }
            throw new ArgumentException ("Invalid command");
        }

        public ICommand Parse (string[] args)
        {
            var command = ParseCommand(args);
            var isArgumentsCorrect = command.CheckArguments(args);
            if (isArgumentsCorrect)
            {
                command.Parse(args);
                return command;
            }

            throw new ArgumentException ("Invalid Arguments");            
        }

        public bool TryParse (string[] args, out ICommand resultCommand)
        {
            resultCommand = ParseCommand(args);
            if (!resultCommand.CheckArguments (args))
            {
                return false;
            }
            return resultCommand.TryParse(args);
        }

        public ICommand HelpCommand ()
        {
            return new HelpCommand();
        }
    }
}
